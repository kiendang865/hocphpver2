-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th2 28, 2019 lúc 02:45 PM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `congnghe`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `id` int(11) NOT NULL,
  `masp` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `tensp` text COLLATE utf8_unicode_ci NOT NULL,
  `gia` int(9) NOT NULL,
  `mota` text COLLATE utf8_unicode_ci NOT NULL,
  `hinhanh` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`id`, `masp`, `tensp`, `gia`, `mota`, `hinhanh`) VALUES
(1, 'SP01', 'DELL', 2000000, 'Core i7 8550U 16GB SSD 512GB LCD 13.3\" UHD 4K TOUC', '3.jpg'),
(2, 'SP02', 'ACER', 5000000, 'Acer Aspire E5-576G-54JQ NX.GRQSV.001 - Xám - Intel® Core i5-8250U (1.6GHz Upto 3.4GHz, 4 Cores 8 Threads, 6MB Cache), 4GB DDR3L 1600Mhz, 1TB HDD 5400rpm, Nvidia Geforce MX150 2GB GDDR5, 15.6\" FullHD (1920x1080) LED, DVDRW, Webcam, Wlan ac +BT, sd card reader, 4 Cell, 2.1kg, Linux', '4.jpg'),
(3, 'SP03', 'MACBOOK PRO', 1000000, 'á á á á', '5.jpg');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
